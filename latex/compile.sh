#!/bin/bash

#stop script if a command fails
set -e

# Generate article
# Delete temporary files
rm -f *.aux
pdflatex article.tex
biber article.bcf
pdflatex article.tex
