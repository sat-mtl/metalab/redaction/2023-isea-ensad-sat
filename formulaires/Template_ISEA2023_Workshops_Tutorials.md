ISEA2023 Call for Proposals {#isea2023-call-for-proposals .Titre}
===========================

**WORKSHOP AND TUTORIAL**

We invite proposals for half-, full- or two-day workshops and tutorials
to take place during two days of ISEA2023. On the one hand, these are
intended as opportunities for academics and practitioners to discuss and
work on specific topics, while on the other, they are also appropriate
for engaging with the general public based on local traditional culture.
While workshop organizers are invited to consider the themes of
ISEA2023, proposals on all matters relevant to ISEA community and beyond
are welcome. Accepted workshops will be announced on the symposium
website, and organizers may release their own call for submissions.
Workshop organizers may request a presentation slot during the
conference to report on the work done in their respective workshops and
may submit a one-page abstract of their workshop proposal to be included
in the proceedings. There may also be opportunities for exhibiting the
outcomes of workshops and tutorials. While all accepted workshops will
be provided with a room, specific needs for space and equipment can be
further discussed.

Application documents should be delivered to the ISEA2023 Organizer

You should join the official ISEA2023 website and upload all the
required documents via online application page.

**Required Documents**

Application Form

-   Workshop/Tutorial Basic Information

-   Short Biography of the Organizers

-   Abstract

-   Plan of How to Solicit Participation

-   List of Equipment

-   Your preference to possibly exhibit the outcomes of the workshop
    during the symposium

> \* To propose workshop and tutorial, submit the proposal of maximum 4
> pages on the ISEA2023 template.
>
> \*Should be submitted in PDF format. (Including a URL link of the
> tutorial website in the PDF file.)
>
> \*All documents should be prepared in English.
>
> \*If you are receiving any support from another institution or under
> any arrangement with another institution, such as a development plan,
> and the period of such support or arrangement overlaps with the
> proposed project, please provide a brief description of the support or
> arrangement in this form.

For technical questions about Microsoft Word formatting please seek
online tutorials. For other questions about your manuscript please
contact:
[[submission.isea2023\@gmail.com]{.underline}](mailto:submission.isea2023@gmail.com)

ISEA2023 Workshop/Tutorial Application Form {#isea2023-workshoptutorial-application-form .Titre}
===========================================

1.  **Workshop/Tutorial Basic Information**

+-----------------------------------+-----------------------------------+
| Type                              | Workshop X / Tutorial            |
+===================================+===================================+
| Title of the                      |                                   |
|                                   |                                   |
| Workshop/ Tutorial                |                                   |
+-----------------------------------+-----------------------------------+
| Projected Duration                | Half Day  / Full Day  / Two     |
|                                   | Days (Workshop Only) X            |
+-----------------------------------+-----------------------------------+
| Name of the Organizers,           |  EnsadLab, [SAT] Metalab          |
| Presenters & Affiliations         |                                   |
+-----------------------------------+-----------------------------------+
| Rationale / Target audience of    |                                   |
| the Workshop                      |                                   |
+-----------------------------------+-----------------------------------+
| URL of                            |                                   |
|                                   |                                   |
| the tutorial website              |                                   |
+-----------------------------------+-----------------------------------+

2.  **Short Biography of the Organizers (within 300 words)**

  ----------------------------------------------------------------------------------------
  The text body of short biography should be 12 point. Please fill in maximum 300 words.
  ----------------------------------------------------------------------------------------

  Founded in 2002, Metalab is the research laboratory of the Society for Arts and Technology [SAT]. Metalab's mission is twofold: to stimulate the emergence of innovative immersive experiences and to make their design accessible to artists and creators of immersion through an ecosystem of free software that addresses problems that are not easily solved by existing tools. The software (and recently hardware) developed at the Metalab is often used with other existing technologies to facilitate the mixing of video mapping, 3D audio, telepresence and interaction.

  Training a new generation of artists and designers through research, graduating them with a practice-based doctorate and developing the research activity itself and its valorisation, these are the three objectives of EnsadLab, EnsAD's laboratory. A pioneer at the time of its creation in 2007, EnsadLab has always had a vocation to be ahead of its time.
  Reflective Interaction is an art and design research group of EnsadLab focusing on interactive “dispositifs”. Among others, The Reflective Interaction group addresses this question : why and how might we design, produce and explore artistic interactive dispositifs that articulate the aesthetic, symbolic and operative dimensions, to engender experiences that are both sensitive and reflective?

3.  **Abstract **

  ---------------------------------------------------------------------------------
  The text body of Abstract should be 12 point. Please fill in maximum 500 words.
  ---------------------------------------------------------------------------------

  This workshop aims to explore different uses of interactive technologies consisting of co-located, collaborative interaction within an artistic installation. We will be using a technology stack of our own brew in order to create an immersive artistic experience solliciting audience participation. The participants will be asked to play along. The goal of the workshop is two-fold: on one hand, to allow the participants to partake in a unique experience and explore different interaction modalities, on the other hand, to open up a conversation about the many faces of technologies that allow for tracking users' actions, presence, attention, intentions, etc.
  We will make use of [Maison du geste et de l'image](http://www.mgi-paris.org/) to deploy our technology stack consisting of video mapping software and hardware, spatial audio setup, video tracking technology, and software for handling interaction via mobile phones to explore themes of symbiosis between humans and machines, humans and other humans, surveillance and control, collaboration, cooperation and competition.
  The participants will be invited to use their own mobile devices to participate in the experience (on a local network) and take part in converstaions resulting from the experience.

4.  **Plan of How to Solicit Participation**

  -----------------------------------
  The text body should be 12 point.
  -----------------------------------

  We will publish a website with a detailed description of the workshop. The ISEA organisers will be able to communicate the workshop content to other ISEA participants. We will communicate later about the maximum number of participants

5.  **List of Equipment**

  -----------------------------------
  The text body should be 12 point.
  -----------------------------------

All equipement and space will be assumed by the workshop authors. The workshop will take place at MGI.

6.  **Your preference to possibly exhibit the outcomes of the workshop
    during the symposium**

  -----------------------------------
  The text body should be 12 point.
  -----------------------------------

  We will welcome non-participating audience at any time of the workshop.
