ISEA 2023 - Notes
=================

## Article

* article court (2-4 pages) ou long (5-8 pages) selon ce que l’on arrive à écrire
* rédaction en Markdown
* piste :
  * une démarche croisée artistique/technologique nous amène à proposer des éléments de réponse à certains questionnements (mentionnés dans la section Installation / œuvre), sans que ce soit l’idée de départ.

## Installation / Œuvre

### Thèmes clés

* expérience in situ
* proximité
* vie privée
* symbiose / ambivalence

### Inspirations

* Journey (le jeu vidéo)
* déplacer un objet commun en faisant une “moyenne” des mouvements des participants

### Technologies possiblement mises en œuvre :

* mobilizing.js
* splash
* satie
* livepose
* glue autour de tout ça

### Discours

* souligner l’interdépendance entre les technologies et l’intention artistique
* illustration du processus de création sur la base de l’expérimentation ?
* différence design artistique / HCI : HCI n’aborde pas l’impact perceptif de la technologie, juste son utilisabilité
* récolte de logs pour construire une connaissance des mécanismes soutenant les interactions de groupe
* questionner l’utilisation des outils open/blackbox, transparence

### Format

* interaction collaborative : mettre en lumière ceux qui n’ont pas joué le jeu du groupe à la fin de l’expérience
* approches d’interaction de petits groupes vs. larges groupes
    * micro-scenarios
